variable "cidrblock-vpc" {
  default = "194.170.0.0/16"
}

variable "subnetip" {
  type = map
  default = {
    "subnet-public" = "194.170.0.0/24"
    "subnet-prive"  = "194.170.1.0/24"
  }

}

variable "availabilityzone" {
  type = map
  default = {
    "zone prive"  = "us-east-1a"
    "zone public" = "us-east-1b"
  }

}