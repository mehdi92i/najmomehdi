resource "aws_vpc" "vpcarpej" {
  cidr_block = var.cidrblock-vpc

  tags = {
    Name = "vpc-arpej"
  }

}
resource "aws_subnet" "subnnet-test1" {
  vpc_id            = aws_vpc.vpcarpej.id
  cidr_block        = lookup(var.subnetip, "subnet-public")
  availability_zone = lookup(var.availabilityzone,"zone public")
  tags = {
    Name = "sous-reseau-public"
 }

}
resource "aws_subnet" "subnettest2" {
    vpc_id            = aws_vpc.vpcarpej.id
    cidr_block        = lookup(var.subnetip, "subnet-prive")
    availability_zone = lookup(var.availabilityzone,"zone prive")
    tags = {
    Name = "sous-reseau-prive"
 }

  
}

resource "aws_internet_gateway" "pub-vpc-internet" {
  vpc_id = "${aws_vpc.vpcarpej.id}"
  tags = {
    Name = "passerelle-internet-vpc-pub"
  }
}

resource "aws_route" "route" {
  route_table_id = "${aws_vpc.vpcarpej.default_route_table_id}"
  gateway_id = "${aws_internet_gateway.pub-vpc-internet.id}"
  destination_cidr_block = "0.0.0.0/24"
}
resource "aws_security_group" "sshaccess" {
  vpc_id = "${aws_vpc.vpcarpej.id}"
  name = "ssh-acces-22"
  ingress {
    cidr_blocks = ["0.0.0.0/0"] 
    description = "value"
    from_port = "22"
    protocol = "tcp"
    to_port = "22"
  }
  egress {
    from_port = "0"
    to_port = "0"
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}

resource "aws_instance" "adjou-test" {
  ami           = "ami-0574da719dca65348"
  instance_type = "t2.micro"
  vpc_security_group_ids = ["${aws_security_group.sshaccess.id}"]
  subnet_id = "${aws_subnet.public-sousreseau.id}"
  tags = {
    Name = "adjou-test-nouveau"
    Description = "essaie de création un ec2 dans vpc vpc-test"   
  }
}
